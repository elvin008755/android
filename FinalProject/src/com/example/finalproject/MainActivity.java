package com.example.finalproject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;



public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //text code
        String city[];
        List <String> list = new ArrayList<String>();
        setContentView(R.layout.fragment_main);
        String str = "<font color='#FFFFFF'>REMEMBER</font><br> <font color='#C0C0C0'>MY FACE!</font>";
        ((TextView) findViewById(R.id.textView2))
        .setText(Html.fromHtml(str));
        
        //json
        
        String JSONString = null;
        JSONObject JsonObj = null;
        try {

            //open the inputStream to the file 
            InputStream inputStream = getAssets().open("map.json");

            int sizeOfJSONFile = inputStream.available();

            //array that will store all the data 
            byte[] bytes = new byte[sizeOfJSONFile];

            //reading data into the array from the file
            inputStream.read(bytes);

            //close the input stream
            inputStream.close();
           
            JSONString = new String(bytes, "UTF-8");
            JsonObj = new JSONObject(JSONString);
            JSONObject JsonObj2; 
            JSONObject JsonObj3; 
            city = new String[JsonObj.getJSONArray("region").length()];
            JsonObj2 = new JSONObject();
            for(int i = 0; i < JsonObj.getJSONArray("region").length();i++){
            	JsonObj2 = new JSONObject(JsonObj.getJSONArray("region").get(i).toString());
            	for(int j = 0; j < JsonObj2.getJSONArray("cityInfo").length();j++){
            		JsonObj3 = new JSONObject(JsonObj2.getJSONArray("cityInfo").get(j).toString());		
                	list.add(JsonObj3.getString("name"));
            	}
  
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
       
        }
        catch (JSONException x) {
            x.printStackTrace();
          
        }
        
        //Spinner code
        
        Spinner s = (Spinner) findViewById(R.id.Spinner01);
        
        ArrayAdapter spinnerData =  new ArrayAdapter(this, R.layout.spinner_item, list);

        s.setAdapter(spinnerData);
        spinnerData.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        	
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            
            return rootView;
        }
    }
}
